function psmin() {
	if [[ -v _PS1$$ ]]; then
		export PS1="${(P)$(<<<_PS1$$)}"
		export RPS1="${(P)$(<<<_RPS1$$)}"
		unset _PS1$$
		unset _RPS1$$
	else
		export _PS1$$="$PS1"
		export _RPS1$$="$RPS1"

		if [[ -v PSMIN ]]; then
			export PS1="$PSMIN"
			export RPS1="$RPSMIN"
		else
			export PS1="%(!.$.#)> "
			export RPS1="%1~"
		fi
	fi
}
